#include "tp1.h"
#include <QApplication>
#include <time.h>

int isMandelbrot(Point z, int n, Point point){
    // recursiv Mandelbrot

    float module = sqrt((z.x)*(z.x) + (z.y)*(z.y));

    if (module > 2) {
        return 1;
    }
    else if (n == 0) {
        return 0;
    }
    else {
        float zx = z.x;
        z.x = (z.x)*(z.x) - (z.y)*(z.y) + point.x;
        z.y = 2 * (zx) * (z.y) + point.y;
        return isMandelbrot(z, n-1, point);
    }
    // check n

    // check length of z
    // if Mandelbrot, return 1 or n (check the difference)
    // otherwise, process the square of z and recall
    // isMandebrot
    return 0;
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow* w = new MandelbrotWindow(isMandelbrot);
    w->show();

    a.exec();
}



