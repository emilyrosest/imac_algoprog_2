#include <iostream>

using namespace std;

struct Noeud{
    int donnee;
    Noeud* suivant;
};

struct Liste{
    Noeud* premier;
    // your code
};

struct DynaTableau{
    int* donnees;
    // your code
    int nombreElements;
    int tailleMax;
    int* tab;
};


void initialise(Liste* liste)
{

}

bool est_vide(const Liste* liste)
{
    return false;
}

void ajoute(Liste* liste, int valeur)
{
    Noeud *nouveau = new Noeud;
    nouveau -> donnee = valeur;
    nouveau -> suivant = nullptr;

    Noeud *noeudActuel = liste -> premier;
    while (noeudActuel -> suivant != nullptr) {
        noeudActuel = noeudActuel -> suivant;
    }
    noeudActuel -> suivant = nouveau;

}

void affiche(const Liste* liste)
{
    Noeud* noeudActuel = liste->premier;
    while (noeudActuel -> suivant != nullptr) {
        cout << noeudActuel -> donnee << endl;
        noeudActuel = noeudActuel -> suivant;
    }
}

int recupere(const Liste* liste, int n)
{
    if (n == 0) {
        return liste -> premier -> donnee;
    }
    else {
        Noeud* noeudActuel = liste->premier;
        for (int i = 0; i < n; i++) {
            noeudActuel = noeudActuel -> suivant;
        }
        return noeudActuel -> donnee;
    }
}

int cherche(const Liste* liste, int valeur)
{
    int index = 0;
    Noeud* noeudActuel = liste->premier;
    while (noeudActuel -> suivant != nullptr) {
        if (noeudActuel -> donnee == valeur) {
            return index;
        }
        noeudActuel = noeudActuel -> suivant;
        index++;
    }
    return -1;
}

void stocke(Liste* liste, int n, int valeur)
{
    Noeud* noeudActuel = liste->premier;
    for (int i = 0; i <= n; i++) {
        noeudActuel = noeudActuel -> suivant;
    }
    noeudActuel -> donnee = valeur;
}

void ajoute(DynaTableau* tableau, int valeur)
{
    if (tableau -> nombreElements == tableau -> tailleMax) {
        (tableau -> tailleMax)++;
        int* tab2 = new int[tableau -> tailleMax + 1];

        for (int i = 0; i <= tableau -> tailleMax; i++) {
            tab2[i] = tableau -> tab[i];
        }
        tab2[tableau -> nombreElements] = valeur;
        (tableau -> tab) = tab2;

        (tableau -> nombreElements) ++; 
    }
    else {
        tableau -> tab[tableau -> nombreElements] = valeur;
        tableau -> nombreElements ++;
    }
}


void initialise(DynaTableau* tableau, int capacite)
{
    tableau -> tailleMax = capacite;
}

bool est_vide(const DynaTableau* liste)
{
    return false;
}

void affiche(const DynaTableau* tableau)
{
    for (int i = 0; i <= tableau -> nombreElements; i++) {
        cout << tableau -> tab[i] << endl;
    }
}

int recupere(const DynaTableau* tableau, int n)
{
    return tableau -> tab[n+1] ;
}

int cherche(const DynaTableau* tableau, int valeur)
{
    int index = 0;
    for (int i = 0; i<= tableau -> nombreElements; i++) {
        if (tableau -> tab[i] == valeur) {
            return index;
        }
        index++;
    }
    return -1;
}

void stocke(DynaTableau* tableau, int n, int valeur)
{
    tableau -> tab[n] = valeur;
}

//void pousse_file(DynaTableau* liste, int valeur)
void pousse_file(Liste* liste, int valeur)
{
    Noeud *nouveau = new Noeud;
    nouveau -> donnee = valeur;
    nouveau -> suivant = nullptr;
    if (liste -> premier == nullptr) {
        liste -> premier = nouveau;
    }
    else {
        Noeud *noeudFile = liste -> premier;
        while (noeudFile -> suivant != nullptr) {
            noeudFile = noeudFile -> suivant;
        }
        noeudFile -> suivant = nouveau;
    }
}

//int retire_file(Liste* liste)
int retire_file(Liste* liste)
{
    if (liste == nullptr) {
        return 0;
    }
    int donneeRetiree = 0;
    Noeud *noeudRetire = liste -> premier;
    if (liste != nullptr && liste -> premier != nullptr) {
        donneeRetiree = noeudRetire -> donnee;
        liste -> premier = noeudRetire -> suivant;
        delete noeudRetire;
    }
    return donneeRetiree;
}

//void pousse_pile(DynaTableau* liste, int valeur)
void pousse_pile(Liste* liste, int valeur)
{
    Noeud *nouveau = new Noeud;
    nouveau -> donnee = valeur;
    nouveau -> suivant = liste -> premier;
    liste -> premier = nouveau;
}

//int retire_pile(DynaTableau* liste)
int retire_pile(Liste* liste)
{
    if (liste == nullptr) {
        return 0;
    }
    int donneeRetiree = 0;
    Noeud *noeudRetire = liste -> premier;
    if (liste != nullptr && liste -> premier != nullptr) {
        donneeRetiree = noeudRetire -> donnee;
        liste -> premier = noeudRetire -> suivant;
        delete noeudRetire;
    }
    return donneeRetiree;
}


int main()
{
    Liste liste;
    initialise(&liste);
    DynaTableau tableau;
    initialise(&tableau, 5);

    if (!est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (!est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    for (int i=1; i<=7; i++) {
        ajoute(&liste, i*7);
        ajoute(&tableau, i*5);
    }

    if (est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    std::cout << "Elements initiaux:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;

    std::cout << "21 se trouve dans la liste à " << cherche(&liste, 21) << std::endl;
    std::cout << "15 se trouve dans la liste à " << cherche(&tableau, 15) << std::endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements après stockage de 7:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    Liste pile; // DynaTableau pile;
    Liste file; // DynaTableau file;

    initialise(&pile);
    initialise(&file);

    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }

    int compteur = 10;
    while(!est_vide(&file) && compteur > 0)
    {
        std::cout << retire_file(&file) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    compteur = 10;
    while(!est_vide(&pile) && compteur > 0)
    {
        std::cout << retire_pile(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    return 0;
}
