#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w = nullptr;

void selectionSort(Array& toSort){
	// selectionSort

    for (int i = 0; i < toSort.size(); i++){
        int minimum = toSort[i];
        int minimumIndex = i;
        for (int j = i+1; j < toSort.size(); j++){
            if (toSort[j] < minimum){
                minimum = toSort[j];
                minimumIndex = j;
            }
        }
        //toSort[j] = toSort[i];
        toSort.swap(i, minimumIndex);
        //toSort[i] = minimum;
    }
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount=15; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(selectionSort); // window which display the behavior of the sort algorithm
    w->show();

	return a.exec();
}
