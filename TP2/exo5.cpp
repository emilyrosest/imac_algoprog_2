#include <QApplication>
#include <time.h>
#include "tp2.h"

MainWindow* w=nullptr;

void merge(Array& first, Array& second, Array& result);

void splitAndMerge(Array& origin)
{
	// stop statement = condition + return (return stop the function even if it does not return anything)
    if(origin.size() <= 1){
        return;
    }

	// initialisation
	Array& first = w->newArray(origin.size()/2);
	Array& second = w->newArray(origin.size()-first.size());
	
	// split
    for(int i = 0; i < first.size(); i++){
        first[i] = origin[i];
    }
    for(int j = 0; j < second.size(); j++){
        second[j] = origin[j + first.size()];
    }

	// recursiv splitAndMerge of lowerArray and greaterArray
    splitAndMerge(first);
    splitAndMerge(second);


	// merge
    merge(first, second, origin);


}

void merge(Array& first, Array& second, Array& result)
{
    int cursor_first = 0;
    int cursor_second = 0;
    int i = 0;

    while (cursor_first < first.size() && cursor_second < second.size()) {
        if (first[cursor_first] < second[cursor_second]){
            result[i++] = first[cursor_first];
            cursor_first++;
        }
        else {
            result[i++] = second[cursor_second];
            cursor_second++;
        }
    }
    if (cursor_first < first.size()){
        for (int j = cursor_first; j < first.size(); j++){
            result[i++] = first[j];
        }
    }
    else if (cursor_second < second.size()){
        for (int j = cursor_second; j < second.size(); j++){
            result[i++] = second[j];
        }
    }
}

void mergeSort(Array& toSort)
{
    splitAndMerge(toSort);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 50;
    w = new TestMainWindow(mergeSort);
	w->show();

	return a.exec();
}
