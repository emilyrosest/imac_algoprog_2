#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

int Heap::leftChild(int nodeIndex)
{
    return (nodeIndex * 2 + 1);
}

int Heap::rightChild(int nodeIndex)
{
    return (nodeIndex * 2 + 2);
}

void Heap::insertHeapNode(int heapSize, int value)
{
	// use (*this)[i] or this->get(i) to get a value at index i
	int i = heapSize;
    (*this)[i] = value;
    while (i > 0 && this->get(i) > this->get((i-1)/2)){
        swap(i, (i-1)/2);
        i = (i-1)/2;
    }
}

void Heap::heapify(int heapSize, int nodeIndex)
{
	// use (*this)[i] or this->get(i) to get a value at index i
    //int i_max = nodeIndex;
    int largest = nodeIndex;

    if (leftChild(nodeIndex)<heapSize && this->get(nodeIndex) < this->get(leftChild(nodeIndex))){
        largest = leftChild(nodeIndex);
    }
    if (rightChild(nodeIndex)<heapSize && this->get(largest) < this->get(rightChild(nodeIndex))){
        largest = rightChild(nodeIndex);
    }
    if (largest != nodeIndex){
        swap(nodeIndex, largest);
        this->heapify(heapSize, largest);
    }
}

void Heap::buildHeap(Array& numbers)
{
    //construit un tas à partir des valeurs de numbers
    for (int i = 0; i <= numbers.size(); i++){
        insertHeapNode(i, numbers[i]);
    }
}

void Heap::heapSort()
{
    //Construit un tableau trié à partir d'un tas heap
    for (int i = this->size() - 1; i >=0; i--){
        swap(0, i);
        this->heapify(i, 0);
    }
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new HeapWindow();
	w->show();

	return a.exec();
}
