#include "tp3.h"
#include <QApplication>
#include <time.h>

MainWindow* w = nullptr;
using std::size_t;

/**
 * @brief define indexMin and indexMax as the first and the last index of toSearch
 * @param array array of int to process
 * @param toSearch value to find
 * @param indexMin first index of the value to find
 * @param indexMax last index of the value to find
 */
void binarySearchAll(Array& array, int toSearch, int& indexMin, int& indexMax)
{
	// do not use increments, use two different binary search loop

    //rempli l’index mi-nimum et maximum de la valeurtoSearch.
    //Si la valeur n’est pas dans le tableau rempli lesdeux index par−1.

    int start = 0;
    int end = array.size();
    //int mid;
    indexMin = indexMax = -1;


    while (start < end){
        int mid = (start + end) / 2;

        if (toSearch > array[mid]){
            start = mid + 1;
        }
        else if (toSearch < array[mid]){
            end = mid;
        }
        else {
            //end = mid;
            indexMin = mid;
            end = mid;

        }
    }

    start = indexMin;
    end = array.size();

    while (start < end){
        int mid = (start + end) / 2;

        if (toSearch > array[mid]){
            start = mid + 1;
        }
        else if (toSearch < array[mid]){
            end = mid;
        }
        else {
            //start = mid;
            indexMax = mid;
            start = mid + 1;
        }
    }



}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 500;
	w = new BinarySearchAllWindow(binarySearchAll);
	w->show();

	return a.exec();
}
