#include "mainwindow.h"
#include "tp3.h"
#include <QApplication>
#include <time.h>
#include <stack>
#include <queue>

MainWindow* w = nullptr;
using std::size_t;

struct SearchTreeNode : public Node
{    
    SearchTreeNode* left;
    SearchTreeNode* right;
    int value;

    void initNode(int value)
    {
        // init initial node without children

        this->value = value;
    }

	void insertNumber(int value) {
        // create a new node and insert it in right or left child
        //insère un nouveau noeud dans l'arbre avec la valeur value

        SearchTreeNode* newNode = new SearchTreeNode(value);

        if (value < this->value){
            this->left = newNode;
        }
        else {
            this->right = newNode;
        }
    }

	uint height() const	{
        // should return the maximum height between left child and
        // right child +1 for itself. If there is no child, return
        // just 1
        //retourne la hauteur de l'arbre


        if (isLeaf()){
            return 1;
        }

        int heightRight = (this->right == nullptr)? 0 : this->right->height();
        int heightLeft = (this->left == nullptr)? 0 : this->left->height();

        if (heightLeft > heightRight){
            return heightLeft + 1;
        }
        return heightRight + 1;

    }

	uint nodesCount() const {
        // should return the sum of nodes within left child and
        // right child +1 for itself. If there is no child, return
        // just 1
        // retourne le nombre de noeuds dans l'arbre

        if (isLeaf()){
            return 1;
        }

        int countRight = (this->right == nullptr)? 0 : this->right->nodesCount();
        int countLeft = (this->left == nullptr)? 0 : this->left->nodesCount();

        return countRight + countLeft + 1;

	}

	bool isLeaf() const {
        // return True if the node is a leaf (it has no children)
        // retourne vrai si l'arbre est une feuille, faux sinon

        return (this->right != NULL || this->left != NULL);
	}

	void allLeaves(Node* leaves[], uint& leavesCount) {
        // fill leaves array with all leaves of this tree
        // rempli le tableau leaves avec toutes les feuilles de l’arbre


        if (this->isLeaf()){
            leaves[leavesCount] = this;
            leavesCount++;
        }
        else {
            if (this->right != nullptr) {
                this->right->allLeaves(leaves, leavesCount);
            }
            if (this->left != nullptr){
                this->left->allLeaves(leaves, leavesCount);
            }
        }
	}

	void inorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with inorder travel
        // rempli le tableau nodes en parcourant l’arbre dans l’ordre infixe (fils gauche, parent, fils droit)
        if (this->left != nullptr){
            this->left->inorderTravel(nodes, nodesCount);
        }
        if (this->isLeaf()){
            nodes[nodesCount] = this;
            nodes[nodesCount]++;
        }
        if (this->right != nullptr){
            this->right->inorderTravel(nodes, nodesCount);
        }
	}

	void preorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with preorder travel
        // rempli le tableau nodes en parcourant l’arbre dans l’ordre préfixe (parent, fils gauche, fils droit)
        if (this->isLeaf()){
            nodes[nodesCount] = this;
            nodes[nodesCount]++;
        }
        if (this->left != nullptr){
            this->left->inorderTravel(nodes, nodesCount);
        }
        if (this->right != nullptr){
            this->right->inorderTravel(nodes, nodesCount);
        }

	}

	void postorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with postorder travel
        // rempli le tableau nodes en parcourant l’arbre dans l’ordre suffixe (fils gauche, fils droit, parent)
        if (this->left != nullptr){
            this->left->inorderTravel(nodes, nodesCount);
        }
        if (this->right != nullptr){
            this->right->inorderTravel(nodes, nodesCount);
        }
        if (this->isLeaf()){
            nodes[nodesCount] = this;
            nodes[nodesCount]++;
        }
	}

	Node* find(int value) {
        // find the node containing value
        // renvoie le Node associé à value

        if (isLeaf()){
            return nullptr;
        }
        else if (this->value == value){
            return this;
        }
        else if (this->value > value){
            return this->left->find(value);
        }
        else if (this->value < value){
            return this->right->find(value);
        }


    }
    
    void reset()
    {
        if (left != NULL)
            delete left;
        if (right != NULL)
            delete right;
        left = right = NULL;
    }

    SearchTreeNode(int value) : Node(value) {initNode(value);}
    ~SearchTreeNode() {}
    int get_value() const {return value;}
    Node* get_left_child() const {return left;}
    Node* get_right_child() const {return right;}
};

Node* createNode(int value) {
    return new SearchTreeNode(value);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 200;
    w = new BinarySearchTreeWindow<SearchTreeNode>();
	w->show();

	return a.exec();
}
